import Vue from 'vue'
import Router from 'vue-router'

//pages
import Home from './pages/Home.vue'
import Contato from './pages/Contato.vue'
import Skills from './pages/Skills.vue'
import Sobre from './pages/Sobre.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/sobre',
      name: 'sobre',
      component: Sobre
    },
    {
      path: '/skills',
      name: 'skills',
      component: Skills
    },
    {
      path: '/contato',
      name: 'contato',
      component: Contato
    }
  
  ]
})
